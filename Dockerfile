FROM registry.opensuse.org/opensuse/tumbleweed


RUN zypper --non-interactive --no-gpg-checks --ignore-unknown addrepo https://download.opensuse.org/repositories/security:/tls:/unstable/openSUSE_Factory/security:tls:unstable.repo
RUN zypper --non-interactive --no-gpg-checks --ignore-unknown refresh
RUN zypper --non-interactive --no-gpg-checks --ignore-unknown install git pkg-config "pkgconfig(zlib)"
RUN zypper --non-interactive --no-gpg-checks --ignore-unknown install -r security_tls_unstable --force-resolution openssl-3 libopenssl3 libopenssl-3-devel
RUN zypper --non-interactive --no-gpg-checks --ignore-unknown install python311 python311-tox
